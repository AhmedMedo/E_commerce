   @extends('Master1')
<!--///////////////////////to show  t-shirt details//////////////////-->
@section('content')
<section id="main-content">
      <section class="wrapper site-min-height">
       <h3 style="color:#70c745;">Category Table</h3>
	   <!--start ADD button for model -->
         <!-- <input type="button" value="ADD ITEM"  style=" background-color:#000;color:#70c745; float:right; margin-right:0px;"  class=" btn btn-dark" data-toggle="modal" data-target="#m">
          <!--end ADD button for model -->
           <ul class="nav  top-menu" style="  float:none; padding-left:80%">
          <li style=" float: none;   width: 5%;margin: auto; color:#70c745;"><a href="add3">add</a></li></ul>
		  <div style="clear:both;"></div>
		  <!--start category table -->
          <table class="table table-hover table-bordered" style="width:100%; text-align:center; margin-top:10px; background-color:#fff; color:#000;" id="myTable">
           <tr>
           <th>ID-category</th>
           <th>Category-name</th>
           <th>Delete-Row</th>
           <th>Edit-Row</th>
           </tr>
           @foreach($cat as $mycat )
           <tr>
           <td>{{$mycat->id}}</td>
           <td>{{$mycat->cat_name}}</td>
		   <!--start delete button  -->
           <td><!--<input type="button" value="Delete" href="delete2/{{$mycat->idcat}}" style=" background-color:           #000;color:#70c745;"  class=" btn btn-dark">-->
                <ul class="nav  top-menu" style="  float: none; ">
          <li style=" float: none;   width: 40%;margin: auto; color:#70c745;"><a href="delete2/{{$mycat->id}}">delete</a></li>
        </ul>
               </td>
           <!--end delete button  -->
		   <!--start Edit button for model -->
		   <td><!--<input type="button" value="Edit"  style="color:#70c745; background-color:#000;"  class=" btn btn-dark" data-toggle="modal" data-target="#m1">-->
               <ul class="nav  top-menu" style="  float: none; ">
          <li style=" float: none;   width: 40%;margin: auto; color:#70c745;"data-toggle="modal" data-target="#m1"><a >edit</a></li></ul></td>
           <!-- end edit button for model -->
		   </tr>
           @endforeach
           
          </table>
          <!-- end table -->
		  <!--start modle for edit row -->
          <div class="modal fade" id="m1">
          <div class="modal-dialog"><div class="modal-content">
<div class="modal-header" style="color:#000; background-color:#fff;">
<h4 class="modal-title" style="color:#000;"> Edit form </h4>
<button type="button" class="close" data-dismiss="modal">&times;</button></div>
<div class="modal-body"> 
<!-- start edit form -->
          <form action="" method="post" class="" style="width:80%; text-align:left;">
          <div class="form-group">
 <label>ID-category:</label>
 <input type="text" class="form-control form-control-sm"></div>
 <div class="form-group">
 <label>Category-name:</label>
 <input type="text" class="form-control form-control-sm"></div>
 
<div class="modal-footer">
 <input type="submit" value="Update"style="color:#70c745; background-color:#000; margin-top:px; float:right;"  class="btn">
    </div> </form></div>
		 <!-- end edit form -->
		 </div>
          </div>
          </div>
          <!-- end edit model -->
         
          <!-- start add model -->
         
          
          <div class="modal fade" id="m">
          <div class="modal-dialog"><div class="modal-content">
<div class="modal-header" style=" background-color:#fff;">
<h4 class="modal-title" style="color:#000;"> Add form </h4>
<button type="button" class="close" data-dismiss="modal">&times;</button></div>
<div class="modal-body"> 
<!-- start add form -->
         </div>
		 <!--end ADD form -->
		 </div>
          </div>
          </div>
          <!--end ADD model -->
      </section>
      <!-- /wrapper -->
    </section>
@stop