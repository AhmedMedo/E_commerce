@extends('Master1')
<!--///////////////////////to show  t-shirt details//////////////////-->
@section('content')
<!--main content start-->
    <section id="main-content">
      <section class="wrapper site-min-height">
        <h3 style="color:#70c745;">T_SHIRTs Table</h3>
		<!--start ADD button for model -->
         <!-- <input type="button" value="ADD ITEM"  style=" background-color:#000;color:#70c745; float:right; margin-right:0px;"  class=" btn btn-dark" data-toggle="modal" data-target="#m">
          <div style="clear:both;"></div>-->
          <a href="test2" style="float:right;">add</a>
          <!--end ADD button for model -->
		   <!--start t-shirt table -->
		  <table class="table table-hover table-bordered" style="width:100%; text-align:center; margin-top:10px; background-color:#fff; color:#000;" id="myTable">
           <tr>
           <th>ID</th>
           <th>T-shirt-name</th>
           <th>Price</th>
           <th>cat-idcat</th>
           <th>T-shirt-image</th>
           <th>Delete-Row</th>
           <th>Edit-Row</th>
           </tr>
             @foreach($t_shirt as $myt_shirt )
           <tr>
           <td>{{$myt_shirt->id}}</td>
           <td>{{$myt_shirt->title}}</td>
           <td>{{$myt_shirt->price}}</td>
           <td>{{$myt_shirt->cat_idcat}}</td>
           <td>
           <img src="{{asset('/images')}}{{$myt_shirt->img}}" width="100" height="100">
           </td>
		   <!--start delete button  -->
           <td><!--<input type="button" value="Delete" onclick="deleteRow(this)" style=" background-color:           #000;color:#70c745;"  class=" btn btn-dark">-->
               <a href="delete/{{$myt_shirt->id}}">delete</a></td>
           <!--end delete button  -->
		   <!--start Edit button for model -->
		   <td><!--<input type="button" value="Edit"  style="color:#70c745; background-color:#000;"  class=" btn btn-dark" data-toggle="modal" data-target="#m1">-->
               
               <a href="edit/{{$myt_shirt->id}}">edit</a>
               </td>
                      <!-- end edit button for model -->
		   </tr>
            @endforeach
         <!--  <tr>
           <td>2</td>
           <td>pro2</td>
           <td>33$</td>
           <td>1</td>
           <td>
           <img src="images/images.jpg" width="100" height="100">
           </td>
		   <!--start delete button  -->
          <!-- <td><input type="button" value="Delete" onclick="deleteRow(this)" style="color:#70c745; background-color:#000;"  class=" btn btn-dark"></td>
           <!--end delete button  -->
		   <!--start Edit button for model -->
		  <!-- <td><input type="button" value="Edit"  style="color:#70c745; background-color:#000;"  class=" btn btn-dark"data-toggle="modal" data-target="#m1"></td>
                      <!-- end edit button for model -->
		  <!-- </tr>-->
          </table>
          <!-- end table -->
		  <!--start modle for edit row -->

          <!-- end edit model -->
         
          <!-- start add model -->
         
          
          
         
          <!--end ADD model -->
                  
				 
              
            
      </section>
      <!-- /wrapper -->
    </section>
    <!-- /MAIN CONTENT -->
@stop