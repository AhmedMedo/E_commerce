@extends('Master1')
<!--///////////////////////to show  t-shirt details//////////////////-->
@section('content')
<!--main content start-->
    <section id="main-content">
      <section class="wrapper site-min-height">
        <h3 style="color:#70c745;">Color Table</h3>
		<!--start ADD button for model -->
        <!--  <input type="button" value="ADD ITEM"  style=" background-color:#000;color:#70c745; float:right; margin-right:0px;"  class=" btn btn-dark" data-toggle="modal" data-target="#m">--> 
          <a href="add1" style="float:right;">add</a>
<!--end ADD button for model -->         
		 <div style="clear:both;"></div>
		 <!--start color table -->
          <table class="table table-hover table-bordered" style="width:100%; text-align:center; margin-top:10px; background-color:#fff; color:#000;" id="myTable">
           <tr>
           <th>ID-Color</th>
           <th>Color-name</th>
           <th>Delete-Row</th>
           <th>Edit-Row</th>
           </tr>
           @foreach($color as $mycolor )
           <tr>
           <td>{{$mycolor->id}}</td>
           <td>{{$mycolor->color_name}}</td>
		   <!--start delete button  -->
           <td><!--<input type="button" value="Delete" onclick="deleteRow(this)" style=" background-color:           #000;color:#70c745;"  class=" btn btn-dark">-->
               <a href="delete1/{{$mycolor->id}}">delete</a>
               </td>
           <!--end delete button  -->
		   <!--start Edit button for model -->
		   <td><!--<input type="button" value="Edit"  style="color:#70c745; background-color:#000;"  class=" btn btn-dark" data-toggle="modal" data-target="#m1">-->
                <a href="editcolor/{{$mycolor->id}}">edit</a>
               </td>
           <!-- end edit button for model -->
		   </tr>
            @endforeach
          <!-- <tr>
           <td>2</td>
           <td>Black</td>
		   <!--start delete button  -->
          <!--  <td><input type="button" value="Delete" onclick="deleteRow(this)" style="color:#70c745; background-color:#000;"  class=" btn btn-dark"></td>
           <!--end delete button  -->
		   <!--start Edit button for model -->
		<!--    <td><input type="button" value="Edit"  style="color:#70c745; background-color:#000;"  class=" btn btn-dark"data-toggle="modal" data-target="#m1"></td>
           <!-- end edit button for model -->
		  <!--  </tr>-->
          </table>
          <!-- end table -->
		  <!--start modle for edit row -->
          <div class="modal fade" id="m1">
          <div class="modal-dialog"><div class="modal-content">
<div class="modal-header" style="color:#000; background-color:#fff;">
<h4 class="modal-title" style="color:#000;"> Edit form </h4>
<button type="button" class="close" data-dismiss="modal">&times;</button></div>
<div class="modal-body"> 
<!-- start edit form -->
          <form action="" method="post" class="" style="width:80%; text-align:left;">
          <div class="form-group">
 <label>ID-Color:</label>
 <input type="text" class="form-control form-control-sm"></div>
 <div class="form-group">
 <label>Color-name:</label>
 <input type="text" class="form-control form-control-sm"></div>
 
<div class="modal-footer">
 <input type="submit" value="Update"style="color:#70c745; background-color:#000; margin-top:px; float:right;"  class="btn">
         </div> </form></div>
		 <!-- end edit form -->
		 </div>
          </div>
          </div>
          <!-- end edit model -->
         
          <!-- start add model -->
         
          
          <div class="modal fade" id="m">
          <div class="modal-dialog"><div class="modal-content">
<div class="modal-header" style=" background-color:#fff;">
<h4 class="modal-title" style="color:#000;"> Add form </h4>
<button type="button" class="close" data-dismiss="modal">&times;</button></div>
<div class="modal-body"> 
<!-- start add form -->
          <form action="" method="post" class="" style="width:80%; text-align:left;">
          <div class="form-group">
 <label>ID-Color:</label>
 <input type="text" class="form-control form-control-sm"></div>
 <div class="form-group">
 <label>Color-name:</label>
 <input type="text" class="form-control form-control-sm"></div> 
  

<div class="modal-footer">
 <input type="submit" value="ADD"style="color:#70c745; background-color:#000; margin-top:px; float:right;"  class="btn">
         </div> </form></div>
		 <!--end ADD form -->
		 </div>
          </div>
          </div>
		  <!--end ADD model -->
      </section>
      <!-- /wrapper -->
    </section>
    <!-- /MAIN CONTENT -->
@stop