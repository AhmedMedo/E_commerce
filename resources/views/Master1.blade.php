<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="Dashboard">
  <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
  <title>SunFrog - Bootstrap Admin Template</title>

  <!-- Favicons -->
  <link href={{asset('img/favicon.png" rel="icon')}}>
  <link href={{asset('img/apple-touch-icon.png')}} rel="apple-touch-icon">

  <!-- Bootstrap core CSS -->
  <link href={{asset('lib/bootstrap/css/bootstrap.min.css')}} rel="stylesheet">
  <!--external css-->
  <link href={{asset('lib/font-awesome/css/font-awesome.css')}} rel="stylesheet" >
  <!-- Custom styles for this template -->
  <link href={{asset('css/style.css')}} rel="stylesheet">
  <link href={{asset('css/style-responsive.css')}} rel="stylesheet">
  <script src={{asset('lib/chart-master/Chart.js')}}></script>

  <!-- =======================================================
    Template Name: Dashio
    Template URL: https://templatemag.com/dashio-bootstrap-admin-template/
    Author: TemplateMag.com
    License: https://templatemag.com/license/
  ======================================================= -->
  <style>
  .upload-btn-wrapper {
  position: relative;
  overflow: hidden;
  display: inline-block;
}

.butn {
  border: 2px solid gray;
  color: gray;
  background-color: white;
  padding: 8px 20px;
  border-radius: 8px;
  font-size: 20px;
  font-weight: bold;
}

.upload-btn-wrapper input[type=file] {
  font-size: 100px;
  position: absolute;
  left: 0;
  top: 0;
  opacity: 0;
}
  </style>
</head>

<body>
  <section id="container">
    <!-- **********************************************************************************************************************************************************
        TOP BAR CONTENT & NOTIFICATIONS
        *********************************************************************************************************************************************************** -->
    <!--header start-->
    <header class="header black-bg">
      <div class="sidebar-toggle-box">
        <div class="fa fa-bars tooltips" data-placement="right" data-original-title="Toggle Navigation"></div>
      </div>
      <!--logo start-->
      <a href="index.html" class="logo"><b>SUN<span>FROG</span></b></a>
      <!--logo end-->
      <div class="nav notify-row" id="top_menu">
        <!--  notification start -->
        <ul class="nav top-menu">
          
          <!-- notification dropdown start-->
          <li id="header_notification_bar" class="dropdown">
            <a data-toggle="dropdown" class="dropdown-toggle" href="index.html#">
              <i class="fa fa-bell-o"></i>
              <span class="badge bg-warning">7</span>
              </a>
            <ul class="dropdown-menu extended notification">
              <div class="notify-arrow notify-arrow-yellow"></div>
              <li>
                <p class="yellow">You have 7 new notifications</p>
              </li>
              <li>
                <a href="index.html#">
                  <span class="label label-danger"><i class="fa fa-bolt"></i></span>
                  Server Overloaded.
                  <span class="small italic">4 mins.</span>
                  </a>
              </li>
              <li>
                <a href="index.html#">
                  <span class="label label-warning"><i class="fa fa-bell"></i></span>
                  Memory #2 Not Responding.
                  <span class="small italic">30 mins.</span>
                  </a>
              </li>
              <li>
                <a href="index.html#">
                  <span class="label label-danger"><i class="fa fa-bolt"></i></span>
                  Disk Space Reached 85%.
                  <span class="small italic">2 hrs.</span>
                  </a>
              </li>
              <li>
                <a href="index.html#">
                  <span class="label label-success"><i class="fa fa-plus"></i></span>
                  New User Registered.
                  <span class="small italic">3 hrs.</span>
                  </a>
              </li>
              <li>
                <a href="index.html#">See all notifications</a>
              </li>
            </ul>
          </li>
          <!-- notification dropdown end -->
        </ul>
        <!--  notification end -->
      </div>
      <div class="top-menu">
        <ul class="nav pull-right top-menu">
          <li><a class="logout" href="login.html">Logout</a></li>
        </ul>
      </div>
    </header>
    <!--header end-->
    <!-- **********************************************************************************************************************************************************
        MAIN SIDEBAR MENU
        *********************************************************************************************************************************************************** -->
    <!--sidebar start-->
    <aside>
      <div id="sidebar" class="nav-collapse ">
        <!-- sidebar menu start-->
        <ul class="sidebar-menu" id="nav-accordion">
          <p class="centered"><a href="profile.html"><img src={{asset('T.png')}} class="img-circle " width="80"></a></p>
          <h5 class="centered"><span style="color:#70c745;">Sun</span>frog</h5>
          <li class="mt">
            <a href="admin.html">
              <i class="fa fa-dashboard"></i>
              <span>Dashboard</span>
              </a>
          </li>
          
         
          <li class="sub-menu">
            <a href="javascript:;">
              <i class="fa fa-th"></i>
              <span>Data Tables</span>
              </a>
            <ul class="sub">
              <li><a href="admin table.html">Admin Table</a></li>
              <li><a href="category table.html">Category Table</a></li>
              <li><a href={{asset('color/colorshow.blade.php')}}>Color Table</a></li>
              <li><a href="order table.html">Order Table</a></li>
              <li><a href="size table.html">Size Table</a></li>
              <li><a href="style table.html">Style Table</a></li>
              <li><a href="subcategory table.html">SubCategory Table</a></li>
              <li><a href={{asset('t_shirt/show1.blade.php')}}>T-shirt Table</a></li>
              <li><a href="user table.html">User Table</a></li>
            </ul>
          </li>
        </ul>
        <!-- sidebar menu end-->
      </div>
    </aside>
    <!--sidebar end-->
    <!-- **********************************************************************************************************************************************************
        MAIN CONTENT
        *********************************************************************************************************************************************************** -->

    <!--main content end-->
       @yield('content')
    <!--footer start-->
    <footer class="site-footer">
      <div class="text-center">
        <p>
          &copy; Copyrights <strong>SunFrog</strong>. All Rights Reserved
        </p>
        <a href="admin.html#" class="go-top">
          <i class="fa fa-angle-up"></i>
          </a>
      </div>
    </footer>
    <!--footer end-->
  </section>
  <!-- js placed at the end of the document so the pages load faster -->
  <script src={{asset('lib/jquery/jquery.min.js')}}></script>
  <script src={{asset('lib/bootstrap/js/bootstrap.min.js')}}></script>
  <script  src={{asset('jquery.dcjqaccordion.2.7.js')}}></script>
  <script src={{asset('jquery.scrollTo.min.js')}}></script>
  <script src={{asset('jquery.nicescroll.js')}} type="text/javascript"></script>
  <script src={{asset('jquery.sparkline.js')}}></script>
  <!--common script for all pages-->
  <script src={{asset('lib/common-scripts.js')}}></script>
  <!--script for this page-->
  <script src={{asset('lib/sparkline-chart.js')}}></script>



</body>

</html>
