<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class T_shirts extends Model
{
      protected $fillable = [
        "title",
        "price",
        
    ];
    public $table='t_shirts';
    public $timestamps=false;
    public function T_shirts()
    {
        return $this->belongsTo('App\cat');
    }
   public function T_shirts2()
    {
        return $this->belongsToMany('App\size')->using('App\t_shrits_has_size');
    } 
    public function T_shirts3()
    {
        return $this->belongsToMany('App\style')->using('App\t_shrits_has_style');
    }  
    public function T_shirts4()
    {
        return $this->belongsToMany('App\color')->using('App\t_shrits_has_color');
    }  
}

