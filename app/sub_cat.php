<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class sub_cat extends Model
{
    public $table='sub_cat';
    public $timestamps=false;
   public function sub_cat()
    {
        return $this->belongsTo('App\cat');
    }
}
