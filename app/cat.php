<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class cat extends Model
{ 
    public $table='cat';
    public $timestamps=false;
    public function t_shirt()
    {
        return $this->hasMany('App\T_shirts');
    }
    
 public function subcat()
    {
        return $this->hasMany('App\sub_cat');
    }
}