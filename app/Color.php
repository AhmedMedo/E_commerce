<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class color extends Model
{
     public $table='color';
    public $timestamps=false;
    
    public function color ()
    {
        return $this->belongsToMany('App\T_shirts')->using('App\t_shrits_has_color');
    } 
}
