<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cat;
class categorycontroller extends Controller
{
    ///----show the category table from database
    public function cat(){
       
      $cat=Cat::all();
       
       $arr=Array('cat'=>$cat);
       ////  dd($arr);
       return view('cat.category',$arr);
   }
    
    
    ///----show the category-name to select  from database
    public function cat1(){
       
      $cat1=Cat::all();
       
       $arr=Array('cat'=>$cat1);
       ////  dd($arr);
       return view('cat.add3',$arr);
   }
    
    
    ////--------------add colom---------///
     public function addcat(Request $request){
          //only execute when post
       if($request->isMethod('post')){
           
          $newrecord= new Cat();
           $newrecord->cat_name=$request->input('cat_name');
           
               $newrecord->save();
            return redirect("cat.category");
           
       }      
              
              
          
          
         return view('cat.add3');
          
      }
   public function delete2($id){
    
    $dcategory=Cat::find($id);
    $dcategory->delete();
    return redirect("category");
} //end delete
}
