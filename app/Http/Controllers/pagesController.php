<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\T_shirts;
use App\Cat;

class pagesController extends Controller
{
   public function Master()
   {
       
    return view('t_shirt.Master1');
   }
    
    // show database
    
     public function t_shirt(){
       
      $t_shirt=T_shirts::all();
       
       $arr=Array('t_shirt'=>$t_shirt);
       ////  dd($arr);
       return view('t_shirt.show1',$arr);
   }
    // add to database problem!!!!!!!!!!!!!!!!!!1
       public function addt_shirt(Request $request){
          //only execute when post
       if($request->isMethod('post')){
           
          $newrecord= new T_shirts();
           $newrecord->title=$request->input('title');
            $newrecord->price=$request->input('price');
           $newrecord->img=$request->input('img');
            $newrecord->cat_idcat=$request->input('cat_idcat');
               $newrecord->save();
       }      
         
         return view('t_shirt.Add');
          
      }
    //end add
    
    //edit
    public function Edit(Request $request,$id){
         if($request->isMethod('post')){
           
          $t_shirt= T_shirts::find($id);
           $t_shirt->title=$request->input('title');
            $t_shirt->price=$request->input('price');
           $t_shirt->img=$request->input('img');
            $t_shirt->cat_idcat=$request->input('cat_idcat');
               $t_shirt->save();
             return redirect("show1");
       }      else{
        
   $t_shirt=T_shirts::find($id);
       
       $arr=Array('t_shirt'=>$t_shirt);
                 
                 return view('t_shirt.edit',$arr);}
    }
    //end edit
   public function delete($id){
    
    $dtshirt=T_shirts::find($id);
    $dtshirt->delete();
    return redirect("show1");
} //end delete
  
}
