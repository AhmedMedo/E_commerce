<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Color;

class colorcontroller extends Controller
{
    ///----show the color table from database
    public function color(){
       
      $color=Color::all();
       
       $arr=Array('color'=>$color);
       ////  dd($arr);
       return view('color.colorshow',$arr);
   }
    
    ///---------------add row ----------------------//
    public function addcolor(Request $request){
          //only execute when post
       if($request->isMethod('post')){
           
          $newrecord= new Color();
           $newrecord->color_name=$request->input('color_name');
           
               $newrecord->save();
            return redirect("colorshow");
           
       }      
              
              
          
          
         return view('color.add1');
          
      }
    //edit
    public function Edit(Request $request,$id){
         if($request->isMethod('post')){
           
          $color= Color::find($id);
            $color->color_name=$request->input('color_name');
           
             $color->save();
            return redirect("colorshow");;
       }      else{
        
   $color=Color::find($id);
       
       $arr=Array('color'=>$color);
                 
                 return view('color.editcolor',$arr);}
    }
    //end edit
     public function delete1($id){
    
    $dcolor=Color::find($id);
    $dcolor->delete();
    return redirect("colorshow");
} //end delete

}
