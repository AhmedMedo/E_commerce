<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTShritsHasSize extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_shrits_has_size', function (Blueprint $table) {
            $table->integer('size_idsize')->unsigned();
            $table->integer('t_shirt_id')->unsigned();
             $table->foreign('size_idsize')->references('idsize')->on('size');
             $table->foreign('t_shirt_id')->references('id')->on('t_shirt');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_shrits_has_size');
    }
}
