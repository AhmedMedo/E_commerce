<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTShritsHasSubCat extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_shrits_has_sub_cat', function (Blueprint $table) {
            $table->integer('sub_cat_idsub_cat')->unsigned();
            $table->integer('sub_cat_cat_idcat')->unsigned();
            $table->integer('t_shirt_id')->unsigned();
             $table->foreign('sub_cat_idsub_cat')->references('idsub_cat')->on('sub_cat');
             $table->foreign('sub_cat_cat_idcat')->references('cat_idcat')->on('sub_cat');
             $table->foreign('t_shirt_id')->references('id')->on('t_shirt');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_shrits_has_sub_cat');
    }
}
