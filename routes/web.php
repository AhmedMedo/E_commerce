<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return view('welcome');
});
//Ezay etnin routes nafs el esm !!! 8alat kda
Route::get('/show', 't_shirtController@t_shirt');
Route::get('/insert', 't_shirtController@addt_shirt');
Route::post('/insert', 't_shirtController@addt_shirt');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

//----------------------new sunfrog1 ----------------//
Route::get('/Master1', 'pagesController@Master');
//show data from database
Route::get('/show1', 'pagesController@t_shirt');
//end show




//delete from database
Route::get('delete/{id}','pagesController@delete');

//---------------test---------------------------//
//test add correct
Route::get('/test2', 'test@addt_shirt');
Route::post('/test2', 'test@addt_shirt');
//end test

//test edit
Route::get('/edit/{id}', 'pagesController@Edit');
Route::post('/edit/{id}', 'pagesController@Edit');

// 
Route::get('/test2', 'test@cat1');


//----------------color----------------------------//
//show from database color table
Route::get('/colorshow', 'colorController@color');
///// add cloum to color table
Route::get('/add1', 'colorcontroller@addcolor');
Route::post('/add1', 'colorcontroller@addcolor');

//delete from database
Route::get('delete1/{id}','colorcontroller@delete1');
// edit
Route::get('/editcolor/{id}', 'colorcontroller@Edit');
Route::post('/editcolor/{id}', 'colorcontroller@Edit');



//////-------------table category start------------------////////
//show from database category table
Route::get('/category', 'categorycontroller@cat');


//delete from database
Route::get('delete2/{idcat}', 'categorycontroller@delete2');


///// add cloum to color table
Route::get('/add3', 'categorycontroller@cat1');
//Route::post('/add3', 'categorycontroller@addcat');




